FROM atlas/analysisbase:21.2.91
# set a bunch of environment variables based on CI
ARG CI_COMMIT_SHA
ARG CI_COMMIT_REF_SLUG
ARG CI_COMMIT_TAG
ARG CI_JOB_URL
ARG CI_PROJECT_URL
ENV CI_COMMIT_SHA=$CI_COMMIT_SHA
ENV CI_COMMIT_REF_SLUG=$CI_COMMIT_REF_SLUG
ENV CI_COMMIT_TAG=$CI_COMMIT_TAG
ENV CI_JOB_URL=$CI_JOB_URL
ENV CI_PROJECT_URL=$CI_PROJECT_URL
# actually install the RPM
COPY susyskim_ewk3l.rpm /code/susyskim_ewk3l.rpm
RUN sudo rpm -i /code/susyskim_ewk3l.rpm && \
    sudo rm -rf /code/susyskim_ewk3l.rpm && \
    sudo chmod 666 /home/atlas/release_setup.sh && \
    sudo printf '\n# Set up the SusySkimEWK3L code\nsource /usr/WorkDir/${AtlasVersion}/InstallArea/${AnalysisBase_PLATFORM}/setup.sh\necho "Configured SusySkimEWK3L."' >> /home/atlas/release_setup.sh && \
    sudo chmod 644 /home/atlas/release_setup.sh
