#!/bin/bash

### Release setup
source setup_release.sh

### build
BUILD_DIR=$TestArea/../build/

# compile
mkdir -p $BUILD_DIR
cd $BUILD_DIR
cmake $TestArea
make -j8

# set env
source $BUILD_DIR/$CMTCONFIG/setup.sh


### post-build
cd $TestArea
source setup_postBuild.sh
