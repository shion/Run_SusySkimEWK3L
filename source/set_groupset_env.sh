#!/bin/bash

#
# Written by Matthew Gignac (UBC, 2016)
#

# Modify this script as needed
# to contain the information below
# These environment variables will be 
# found and used instead of specifying 
# them each time you want to 
# interact with the group-set

export GROUP_SET_NAME=""
export GROUP_SET_DIR=""
export GROUP_SET_TAG="v2.3"
export SELECTOR="EWK3L"
export GROUP_SET_DEEPCONFIG=""
#export X509_USER_PROXY=$HOME/proxy/x509up_u11935
