#!/bin/bash

source set_groupset_env.sh

#### link
mkdir -p ../run/
cd ../run/
ln -sf ../source/SusySkimEWK3L/scripts/src/

mkdir -p batchBNL
cd batchBNL/
for i in `ls ../../source/SusySkimEWK3L/scripts/batchBNL/*.sh`; do ln -sf $i ; done
for i in `ls ../../source/SusySkimEWK3L/scripts/batchBNL/*.py`; do ln -sf $i ; done
ln -sf ../../source/SusySkimEWK3L/scripts/batchBNL/condor/

mkdir -p ../grid

cd ../../source/
