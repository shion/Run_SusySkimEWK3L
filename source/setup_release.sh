#!/bin/bash

#
# Setup release. Edit here if you want to change the AnalysisBase release.
# Place all the setup scripts (setup*sh) in source/ and always source it from the same place: source/.
#

AB_RELEASE=21.2.91

setupATLAS

if [ -f ./CMakeLists.txt ] || [ ! `cat ./CMakeLists.txt | grep "${AB_RELEASE}" | tr -d " " | head -n 1` ]; then
    echo "Creating the top-level CMakeLists.txt ..."
    asetup AnalysisBase,${AB_RELEASE},here

  # RestFrames requires additions to CMakeList after each asetup
  # (see https://gitlab.cern.ch/MultiBJets/Ext_RestFrames)
    if [ -d "Ext_RestFrames" ] && [ `cat CMakeLists.txt | grep -v Ext_RestFrames` ]; then
	sed -i 's/find_package( AnalysisBase )/find_package( AnalysisBase ) \
  \n\n# Include the externals configuration: \
  \ninclude( Ext_RestFrames\/externals.cmake )/g' CMakeLists.txt
    fi

fi

lsetup "asetup AnalysisBase,${AB_RELEASE}" rucio pyami

localSetupPandaClient
