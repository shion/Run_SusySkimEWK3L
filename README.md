## First time
- `git clone` this with a fresh shell and go to the `Run_SusySkim3L` directory (call this `$Base`).
- Go to `$Base/source/`, edit the AnalysisBase release you want in `setup_release.sh`, and do:
<pre>
    source setup.sh
</pre>
- **Ready to go!** Example of a small local test:
<pre>
    run_xAODNtMaker -F myDAOD.pool.root -writeSkims 1 -writeTrees 1 -MaxEvents 1000 -deepConfig SusySkimEWK3L_R21.conf  -selector EWK3LSelector
</pre>

## Next time you log in
- Go to `$Base/source/` and just:
<pre>
    source setup_eachTime.sh
</pre>

## Run on grid
- Edit the tag for the ntuples (`$GROUP_SET_TAG`) and the selector you want to run on (`$SELECTOR`) in `$Base/source/set_groupset_env.sh` and source it.
- Go to `$Base/source/SusySkimEWK3L/scripts/grid/`, edit the prodcution tag, deep-config, systematics, groupset to run etc., and submit by
<pre>
    . submit.sh
</pre>
